package me.boots.repository

import androidx.lifecycle.liveData
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import me.boots.api.Photo
import me.boots.api.PhotoUrls
import me.boots.api.Resource
import me.boots.api.User

/**
 * Mocked version of repository working as offline
 */
class PhotoRepositoryImpl : PhotoRepository {

    private val photoMap = mapOf(
        "1" to Photo("1","Photo A",
            User("username", "Steve"),
            PhotoUrls("file:///android_asset/daniel-chen.jpg")
        ),
        "2" to Photo("2","Photo B",
            User("username", "Alex"),
            PhotoUrls("file:///android_asset/ethan-haddox.jpg")
        ),
        "3" to Photo("3","Photo C",
            User("username", "Browse"),
            PhotoUrls("file:///android_asset/martin-widenka.jpg")
        ),
    )

    override fun getSearchResultStream(query: String): Flow<PagingData<Photo>> {
        return flow {
            emit(PagingData.from(photoMap.values.toList()))
        }
    }

    override fun getPhotoDetails(photoId: String) = liveData {
        emit(Resource.success(photoMap.getValue(photoId)))
    }
}
