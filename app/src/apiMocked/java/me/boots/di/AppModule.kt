package me.boots.di

import me.boots.api.PhotoService
import me.boots.repository.PhotoRepository
import me.boots.repository.PhotoRepositoryImpl
import me.boots.viewmodels.GalleryViewModel
import me.boots.viewmodels.PhotoDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@JvmField
val appModule = module {

    single { PhotoService.create() }

    single<PhotoRepository> { PhotoRepositoryImpl() }

    viewModel { GalleryViewModel(get()) }

    viewModel { parameters -> PhotoDetailViewModel(get(), parameters.get()) }
}