package me.boots

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.databinding.DataBindingUtil.setContentView
import me.boots.databinding.ActivitySplashScreenBinding

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivitySplashScreenBinding;
    private var mAnimator: ObjectAnimator? = null
    private var mCurrentProgress : Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                mCurrentProgress = getFloat(STATE_PROGRESS)
            }
        }

        mBinding = setContentView(this, R.layout.activity_splash_screen)
        loadSplashScreen()
    }

    private fun loadSplashScreen(){
        mAnimator = ObjectAnimator.ofFloat(mBinding.splashProgress, "progress",
                mCurrentProgress, 1F)
            .apply {
                if (mCurrentProgress > 0){
                    duration = SPLASH_SCREEN_TIME_OUT - (mCurrentProgress * SPLASH_SCREEN_TIME_OUT).toLong()
                } else {
                    duration = SPLASH_SCREEN_TIME_OUT
                }
                doOnEnd {
                    startActivity(
                        Intent(this@SplashScreenActivity, MainActivity::class.java)
                    )
                    finish()
                }
                addUpdateListener {
                    mCurrentProgress = it.animatedValue as Float
                }
                start()
            }

    }

    override fun onPause() {
        super.onPause()
        mAnimator?.pause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putFloat(STATE_PROGRESS, mCurrentProgress)
        }
        super.onSaveInstanceState(outState)
    }

    companion object {
        private const val SPLASH_SCREEN_TIME_OUT : Long = 5000
        private const val STATE_PROGRESS = "progress"
    }
}