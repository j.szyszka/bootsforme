package me.boots.viewmodels

import androidx.lifecycle.ViewModel
import me.boots.repository.PhotoRepository

class PhotoDetailViewModel constructor(
    repository: PhotoRepository,
    photoId: String
) : ViewModel() {
    val result = repository.getPhotoDetails(photoId)
}