package me.boots.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import me.boots.api.Photo
import me.boots.repository.PhotoRepository

class GalleryViewModel  constructor(
    private val repository: PhotoRepository
) : ViewModel() {

    fun searchPhotos(queryString: String): Flow<PagingData<Photo>> =
            repository.getSearchResultStream(queryString).cachedIn(viewModelScope)
}
