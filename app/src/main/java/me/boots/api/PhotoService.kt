package me.boots.api

import me.boots.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PhotoService {

    @GET("photos/{id}")
    suspend fun photoDetails(
        @Path("id") id: String,
        @Query("client_id") clientId: String = BuildConfig.UNSPLASH_ACCESS_KEY
    ): Photo

    @GET("search/photos")
    suspend fun searchPhotos(
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
        @Query("client_id") clientId: String = BuildConfig.UNSPLASH_ACCESS_KEY
    ): SearchResponse

    companion object {
        private const val UNSPLASH_URL = "https://api.unsplash.com/"

        fun create(): PhotoService = Retrofit.Builder()
                .baseUrl(UNSPLASH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PhotoService::class.java)
    }
}
