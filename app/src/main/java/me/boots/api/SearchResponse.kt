package me.boots.api

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @field:SerializedName("total_pages") val totalPages: Int,
    @field:SerializedName("results") val results: List<Photo>
)
