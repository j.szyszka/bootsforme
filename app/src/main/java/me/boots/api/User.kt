package me.boots.api

import com.google.gson.annotations.SerializedName

data class User(
    @field:SerializedName("username") val username: String,
    @field:SerializedName("name") val name: String
)