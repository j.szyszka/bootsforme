package me.boots.api

import com.google.gson.annotations.SerializedName

data class PhotoUrls(
    @field:SerializedName("small") val small: String
)
