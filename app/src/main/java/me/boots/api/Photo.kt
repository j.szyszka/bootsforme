package me.boots.api

import com.google.gson.annotations.SerializedName

data class Photo(
    @field:SerializedName("id") val id: String,
    @field:SerializedName("alt_description") val desc: String,
    @field:SerializedName("user") val user: User,
    @field:SerializedName("urls") val urls: PhotoUrls
)
