package me.boots.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import me.boots.api.Photo
import me.boots.api.Resource

interface PhotoRepository {

    fun getSearchResultStream(query: String): Flow<PagingData<Photo>>

    fun getPhotoDetails(photoId: String): LiveData<Resource<Photo>>
}