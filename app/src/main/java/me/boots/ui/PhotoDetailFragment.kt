package me.boots.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import me.boots.databinding.FragmentPhotoDetailBinding
import me.boots.viewmodels.PhotoDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class PhotoDetailFragment : Fragment() {

    private val args: PhotoDetailFragmentArgs by navArgs()

    private val photoDetailViewModel: PhotoDetailViewModel by viewModel() {
        parametersOf(args.photoId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentPhotoDetailBinding.inflate(inflater, container, false
        ).apply {
            viewModel = photoDetailViewModel
            lifecycleOwner = viewLifecycleOwner
            toolbar.setNavigationOnClickListener { view ->
                view.findNavController().navigateUp()
            }

        }

        return binding.root
    }

}
