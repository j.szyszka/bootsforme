package me.boots.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import me.boots.BuildConfig
import me.boots.databinding.FragmentGalleryBinding
import me.boots.ui.adapters.GalleryAdapter
import me.boots.viewmodels.GalleryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class GalleryFragment : Fragment() {

    private val viewModel: GalleryViewModel by viewModel()

    private var searchJob: Job? = null
    private val adapter = GalleryAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentGalleryBinding.inflate(inflater, container, false)
        binding.photoList.adapter = adapter

        binding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }

        search(BuildConfig.PHOTO_SEARCH_CONTEXT)

        return binding.root
    }

    private fun search(query: String) {
        searchJob = lifecycleScope.launch {
            viewModel.searchPhotos(query).collectLatest {
                adapter.submitData(it)
            }
        }
    }
}
