package me.boots.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import me.boots.api.Photo
import me.boots.databinding.ListItemPhotoBinding
import me.boots.ui.GalleryFragmentDirections

class GalleryAdapter : PagingDataAdapter<Photo, GalleryAdapter.GalleryViewHolder>(GalleryDiffCallback()) {

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        val photo = getItem(position)
        if (photo != null) {
            holder.bind(photo)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        return GalleryViewHolder(ListItemPhotoBinding
            .inflate(LayoutInflater.from(parent.context), parent,false)
        )
    }

    class GalleryViewHolder(
        private val binding: ListItemPhotoBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.setClickListener { view ->
                binding.photo?.let { photo ->
                    view.findNavController().navigate(GalleryFragmentDirections
                        .actionGalleryFragmentToDetailFragment(photo.id))
                }
            }
        }

        fun bind(item: Photo) {
            binding.apply {
                photo = item
                executePendingBindings()
            }
        }
    }
}

private class GalleryDiffCallback : DiffUtil.ItemCallback<Photo>() {

    override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem.id == newItem.id
    }
}
