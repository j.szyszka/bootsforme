package me.boots.repository

import android.util.Log
import androidx.lifecycle.liveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import me.boots.api.*

class PhotoRepositoryImpl constructor(private val service: PhotoService) : PhotoRepository {

    override fun getSearchResultStream(query: String): Flow<PagingData<Photo>> {
        return Pager(
            config = PagingConfig(
                enablePlaceholders = false,
                pageSize = NETWORK_PAGE_SIZE
            ),
            pagingSourceFactory = { PhotoPagingSource(service, query) }
        ).flow
    }

    override fun getPhotoDetails(photoId: String) = liveData (Dispatchers.IO){
        try {
            val response = service.photoDetails(photoId)
            emit(Resource.success(response))
        }catch (e : Exception){
            Log.e("PhotoRepositoryImpl", "Cannot get resources", e)
            emit(Resource.error(null))
        }
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }
}