Boots For Me App
================

Aplication presents 3 simple pages with:
1. [Splash Screen] should be displayed for 5 seconds with custom progress bar
2. [Gallery page] with shoes presentation
3. [Shoes details page] Detailed description of choosed shoes

How It run
==========
Before run please apply Unsplash key delivered in separated way and edit proprty at end of file [gradle.properties](https://gitlab.com/j.szyszka/bootsforme/-/blob/master/gradle.properties)
```
unsplash_access_key=<PLEASE_CHANGE_ME>
```

Install Api Mocked/Production version
```
./gradlew installApiMockedDebug
# or
./gradlew installApiProductionDebug
```



Used features
=============
- MVVM
- Koin
- Kotlin coroutines
- Android Pagging Data v3
- Retrofit
